(defsystem "v-alpm"
	:author "Valenoern"
	:licence "AGPL"
		;; AGPL is necessary because this might run on a server
	:description "attempt at a lisp frontend for libalpm / arch linux package manager (aka pacman)"
		;; note: currently alpm is a misnomer because it actually just calls pacman
	
	:depends-on (
		"uiop"  ; to run commands
		)
	:components (
		(:file "util")   ; common functions
		(:file "Query" :depends-on ("util"))  ; pacman -Q
		(:file "Sync" :depends-on ("util"))   ; pacman -Sy / pacman -Su
		(:file "alpm" :depends-on ("Query" "Sync"))  ; unified package
		))
