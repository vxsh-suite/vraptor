;;; Query.lisp -  pacman -Q
(defpackage :v-refracta.alpm.query
	(:use :common-lisp)
	(:local-nicknames (:util :v-refracta.alpm.util))
	(:export
		#:query
))
(in-package :v-refracta.alpm.query)


(defun query (zst-name) ; {{{
	;; ensure zst-name is lowercase
	(setq zst-name
		(if (symbolp zst-name)
			(string-downcase (string zst-name))
			zst-name))
	;; run command
	(util:run-command-string (format nil "pacman -Q ~a" zst-name))

	#| example:
	sbcl> (v-refracta.alpm.query:query 'sbcl)
	sbcl 2.3.1-1

	sbcl> (v-refracta.alpm.query:query 'sbclbogus)
	error: package 'sbclbogus' was not found
	|#

) ; }}}


;; > QUERY OPTIONS (APPLY TO -Q) ...
;; 1/15 represented here
