;;; Sync.lisp -  pacman -S
(defpackage :v-refracta.alpm.sync
	(:use :common-lisp)
	(:local-nicknames (:util :v-refracta.alpm.util))
	(:export
		#:sync
		#:refresh #:sysupgrade
))
(in-package :v-refracta.alpm.sync)


;;(defmacro defbourne (rest-parameter &key key-word)
;; "bourne" function - function definition where keywords can appear either before or after &rest list but not inside
	;;(defun ()

	;;)
;;)

(defun sync ; {{{
	(first-zst second-zst &rest zst-names  &key refresh force-refresh upgrade  &allow-other-keys)
	;; this function is intended to have a single &rest list of arguments but you technically can't do that, so throw the first two packages onto zst-names as if they did not exist
	(setq zst-names
		(append (list first-zst)
			(if (null second-zst) '() (list second-zst))
			(if (null zst-names)  '() zst-names)))

	(let ((command
		(format nil "sudo pacman -S~a~a~a ~A"
			(if (null refresh)
				""
				;; if requested, always refresh databases
				(if (null force-refresh)
					"y"  "yy"))
			(if
				(or (null upgrade)
					(not (null zst-names)))
					;; "avoid installing packages without upgrading" - arch wiki
				""  "u")
			(if (not (null zst-names))
				""  " --noconfirm")
				;; BUG: noconfirm is not recommended. figure out how to prompt
			zst-names
		)))

	(print command)

	;;(util:run-command-string command)

	#| example:
	sbcl> (v-refracta.alpm.sync:refresh)
	:: Synchronising package databases...
	 core downloading...
	 extra downloading...
	 community downloading...
	|#
	;; BUG: I don't know how to properly get the password so I had to simply unlock sudo outside sbcl to get this demo to run. i know that is not necessarily secure. is there a lisp polkit?
)) ; }}}

(defun refresh (&key force-refresh)  ; refresh if needed
	(sync nil nil :refresh t :force-refresh force-refresh))

(defun sysupgrade ()  ; only upgrade
	(sync nil nil :upgrade t))
