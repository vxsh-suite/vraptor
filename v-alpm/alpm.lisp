;;; alpm.lisp - package to easily access subcommands
(defpackage :v-refracta.alpm
	(:shadowing-import-from :v-refracta.alpm.query
		#:query)
	(:shadowing-import-from :v-refracta.alpm.sync
		#:sync #:refresh #:sysupgrade)
	(:use :common-lisp)
	(:export
		#:query
		#:sync #:refresh #:sysupgrade
))
(in-package :v-refracta.alpm)

;; no new symbols to add, see imported packages
