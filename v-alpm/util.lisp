;;; util.lisp - common functions for all subcommands
(defpackage :v-refracta.alpm.util
	(:use :common-lisp)
	(:export
		#:run-command-string
))
(in-package :v-refracta.alpm.util)


(defun run-command-string (command-string)
	(uiop:run-program command-string
		:output *standard-output* :error-output *error-output*)
)
